<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $cashierRole = Role::where('name', 'cashier')->first();
        $customerRole = Role::where('name', 'customer')->first();

        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password')
        ]);

        $cashier = User::create([
            'name' => 'Cashier User',
            'email' => 'cashier@cashier.com',
            'password' => Hash::make('password')
        ]);

        $customer = User::create([
            'name' => 'Customer User',
            'email' => 'customer@customer.com',
            'password' => Hash::make('password')
        ]);

        $admin->roles()->attach($adminRole);
        $cashier->roles()->attach($cashierRole);
        $customer->roles()->attach($customerRole);
    }
}
