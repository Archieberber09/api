@extends('layouts.app')

@section('dashboard')
    <div class="card-header py-4 mb-3">
    	TRANSACTIONS
    </div>
         <table class="table table-striped table-bordered bg-light text-center">
    	<thead>
    		<tr class="text-center">
    		<th>#</th>
    		<th>WEIGHT</th>
    		<th>POINTS</th>
    		<th>CASHIER</th>
    		<th>CUSTOMER</th>
    		<th>CLAIMED</th>
    		<!-- <th>ACTIONS</th> -->
    	</tr>
    	</thead>
    	<tbody>
    		@forelse($transactions as $index => $transaction)
	    		<tr class="text-center">
	    			<td>{{$index+1}}</td>
	    			<td>{{$transaction->weight}}</td>
	    			<td>{{$transaction->points}}</td>
	    			@foreach($users as $user)
	    				@if($transaction->cashier_id == $user->id)
	    					<td>{{$user->name}}</td>
	    				@endif
	    			@endforeach
	    			
	    			@if($transaction->user_id == null)
	    				<td>N/A</td>
	    			@else
	    				@foreach($users as $user)
		    				@if($transaction->user_id == $user->id)
		    					<td>{{$user->name}}</td>
		    				@endif
		    			@endforeach
	    			@endif
	    			@if($transaction->claimed == 0)
	    				<td>false</td>
	    			@else
	    				<td>true</td>
	    			@endif
	    			<!-- <td class="d-flex justify-content-center">
	    				<form action="/transaction/{{$transaction->id}}/edit" action="POST">
	    					<button class="mx-1"><i class="fas fa-edit"></i></button>
	    				</form>
		    			<button class="mx-1" onclick="myfunction({{ $transaction->id }})"><i class="fas fa-eye"></i></button>
		    			<form action="/transaction/{{ $transaction->id }}" method="POST" class="deleteBtn">
							@csrf
							@method("DELETE")
	    					<button class="mx-1"><i class="fas fa-trash"></i></button>
						</form>
	    			</td> -->
	    		@empty
	    			<td colspan="6">No Available Users</td>
	    		</tr>
    		@endforelse
    	</tbody>
    </table>
    <div class="d-flex"> 
            <div class="mx-auto pagination-users">
                    {!! $transactions->links(); !!}
            </div>
        </div>
@endsection

@section('page-script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

	<script>
		//data-tables
		$(document).ready( function () {
			$('.table').DataTable({
				"bPaginate": false,
				"columnDefs": [
				{ "orderable": false, "targets": 5 }
				],
				 dom: 'Bfrtip',
		        buttons: [
		            'copy', 'csv', 'excel', 'pdf', 'print'
		        ]
			});
		});

		// user deletion
		document.querySelectorAll(".deleteBtn").forEach(function(id){

			id.addEventListener('submit', e=>{
				
				e.preventDefault()
				Swal.fire({
					title: 'Are you sure?',
					text: "It will permanently deleted !",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if(result.value){
						id.submit();
					}
				})

			})
		})

		//user modal for full details
		function myfunction(id){

			let url = "/getuserdata/" + id;
			fetch(url)
			.then(response=>response.json())
			.then(function(data){
				console.log(data);
				document.querySelector("#name").innerText = data.name;
				document.querySelector("#role").innerText = data.role;
				document.querySelector("#email").innerText = data.email;
				document.querySelector("#mobile_number").innerText = data.mobile_number;
				$("#exampleModal").modal("show");
			});	

			
		}
	</script>

@endsection