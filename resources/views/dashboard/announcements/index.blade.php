@extends('layouts.app')

@section('dashboard')
    <div class="card-header d-flex justify-content-between align-items-center py-4 mb-3">
    	USERS
    	<span>
          	<a href="/dashboard/announcements/create" class="btn btn-danger">ADD NEW ANNOUNCEMENT +</a>\
        </span>
    </div>
         <table class="table table-striped table-bordered bg-light text-center">
    	<thead>
    		<tr class="text-center">
    		<th>#</th>
    		<th>ANNOUNCEMENT</th>
    		<th>ACTIONS</th>
    	</tr>
    	</thead>
    	<tbody>
    		@forelse($announcements as $index => $announcement)
	    		<tr>
	    			<td class="text-center">{{$index+1}}</td>
	    			<td class="text-left">{{$announcement->announcement}}</td>
	    			<td class="d-flex justify-content-center">
	    				<form action="/announcement/{{$announcement->id}}/edit" action="POST">
	    					<button class="mx-1"><i class="fas fa-edit"></i></button>
	    				</form>
		    			
		    			<form action="/announcement/{{ $announcement->id }}" method="POST" class="deleteBtn">
							@csrf
							@method("DELETE")
	    					<button class="mx-1"><i class="fas fa-trash"></i></button>
						</form>
						<button class="mx-1" onclick="myfunction({{ $announcement->id }})">Publish</i></button>
	    			</td>
	    		@empty
	    			<td colspan="3" class="text-center">No Available Announcements</td>
	    		</tr>
    		@endforelse
    	</tbody>
    </table>
    <div class="d-flex"> 
            <div class="mx-auto pagination-users">
                    {!! $announcements->links(); !!}
            </div>
        </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body ">
				<div class="d-flex justify-content-center mb-3">
					<div class="col-md-12 text-center" id="announcement"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

	<script>
		//data-tables
		$(document).ready( function () {
			$('.table').DataTable({
				"bPaginate": false,
				"columnDefs": [
				{ "orderable": false, "targets": 2 }
				]
			});
		});

		// user deletion
		document.querySelectorAll(".deleteBtn").forEach(function(id){

			id.addEventListener('submit', e=>{
				
				e.preventDefault()
				Swal.fire({
					title: 'Are you sure?',
					text: "It will permanently deleted !",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if(result.value){
						id.submit();
					}
				})

			})
		})

		//user modal for full details
		function myfunction(id){

			let url = "/getuserdata/" + id;
			fetch(url)
			.then(response=>response.json())
			.then(function(data){
				console.log(data);
				document.querySelector("#name").innerText = data.name;
				document.querySelector("#role").innerText = data.role;
				document.querySelector("#email").innerText = data.email;
				document.querySelector("#mobile_number").innerText = data.mobile_number;
				$("#exampleModal").modal("show");
			});	

			
		}
	</script>

@endsection