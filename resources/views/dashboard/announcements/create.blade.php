@extends('layouts.app')

@section('dashboard')
    <div class="container pt-5">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if (session('success'))
            <div class="alert alert-success text-center">
                <h5 class="mb-0">{{ session('success') }}</h5>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger text-center">
                <h5 class="mb-0">{{ session('error') }}</h5>
            </div>
        @endif

            <div class="card">

                <div class="card-header">{{ __('Create') }}</div>

                <div class="card-body">
                    <form method="POST" action="/create-announcement">
                        @csrf

                        <div class="form-group">
                                <textarea id="announcement" type="text" class="form-control @error('announcement') is-invalid @enderror" name="announcement" value="{{ old('announcement') }}"  autocomplete="announcement" autofocus style="height: 200px;"></textarea>

                                @error('announcement')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save Announcement') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection