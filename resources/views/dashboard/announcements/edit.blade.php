@extends('layouts.app')

@section('dashboard')
    <div class="container pt-5">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">{{ __('Edit Announcement') }}</div>

                <div class="card-body">
                    <form method="POST" action="/announcement/{{$announcement->id}}/edit">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                                <textarea id="announcement" type="text" class="form-control @error('announcement') is-invalid @enderror" name="announcement" value="{{$announcement->announcement}}"  autocomplete="announcement" autofocus style="height: 200px;">{{$announcement->announcement}}</textarea>

                                @error('announcement')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('SAVE UPDATE') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection