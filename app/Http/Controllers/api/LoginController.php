<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword as ForgotPassword;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    public function login(Request $request){
    	$login = $request->validate([
    		'email' => 'string|email|required',
    		'password' => 'string|required'
    	]);

    	if(!Auth::attempt($login)){
    		return response(['message' => 'Invalid email or password!']);
    	}

    	$accessToken = Auth::user()->createToken('authToken')->accessToken;

    	return response(['access_token' => $accessToken]);
    }

    public function forgotpassword(Request $request){
    	$isEmailExist = User::whereEmail($request->email)->first();

    	if($isEmailExist){

    		Mail::to($request->email)->send(new ForgotPassword([
	        'email'  => $request->email,
	        'resetpassword_token' => $isEmailExist->token
	     	 ]));
    		return response(["message" => 'Please check your email for password reset link.', "token" => $isEmailExist->token]);
    	}
    }

    public function resetpassword($id, Request $request){
    	
    	$user = User::whereToken($id)->first();

   		if($user){
   			if($request->newpassword === $request->confirmnewpassword){
   				$user->password = Hash::make($request->newpassword); 
   				$user->save();
   				return response(['message', 'Successfully updated password']);
   			}else{
   				return response(['message', "Password didn't match"]);

   			}
   		}

    }
}
