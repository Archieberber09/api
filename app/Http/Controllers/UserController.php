<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
       public function register(Request $request){
    	$request->validate([
    		'name' => 'required',
    		'email' => 'required|email',
    		'mobile_number' => 'required',
            'password' => 'required|min:8',
    		'role' => 'required'
    	]);

    	$isExists = User::whereEmail($request->email)->first();

    	if(!$isExists){
    		$user = new User;
	    	$user->name = $request->name;
	    	$user->email = $request->email;
	    	$user->role = $request->role;
            $user->mobile_number = $request->mobile_number;
	    	$user->points = 0;
	    	$user->password = Hash::make($request->password);
	    	$user->token = Str::random(15);
	    	$user->save();

            Alert::success('Successful Registration!');
	    	return redirect('dashboard/users');
    	}else{
	    	Alert::error('Email Already Exist!');
            return back();
    	}
    }

    public function getUsers()
    {
        $users = User::orderBy('id', 'DESC')->paginate(10);
        return view('/dashboard/users', compact('users'));
    }

    

    public function edit($id)
    {
        $user = User::whereId($id)->first();
        return view('/dashboard/usermanagement/edit', compact('user'));
    }

    public function saveupdate(User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'mobile_number' => 'required',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile_number = $request->mobile_number;
        $user->role = $request->role;
        
        if ($request->password != "") {
            $user->password = Hash::make($request->password);
        }

        $user->save();
         Alert::success('Successful Profile Update!');

        return redirect('dashboard/users');
    }

    public function destroy(User $user, Request $request)
    {
        $user->delete();
        Alert::success('Successfully Deleted');
        return back();
    }

    public function getuserdata($id){

        $user = User::find($id);

        return response()->json($user); 
    }
}
