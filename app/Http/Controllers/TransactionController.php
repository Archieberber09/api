<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::orderBy('id', 'DESC')->paginate(10);
        $users = User::all();
        return view('dashboard.transactions.index', compact('transactions', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'weight' => 'required'
        ]);

        $transaction = new Transaction;
        $transaction->weight = $request->weight;
        $transaction->points = $request->weight / 2;
        $transaction->cashier_id = Auth::user()->id;
        $transaction->user_id = null;
        $transaction->claimed = false;
        $transaction->save();

        $qrcode = Transaction::orderBy('id','DESC')->first();

        return view('/dashboard/qrcodegenerator', compact('qrcode'));
    }

    public function qrscanned($qrcode, Request $request)
    {
        $transaction = Transaction::whereId($qrcode)->first();

        if($transaction){
           if($transaction->claimed == false){
                $transaction->claimed = true;
                $transaction->save();
                return response(['message' =>  'Scan successful'], 200);
           }else{
            return response(['error' =>  'Points Already Claimed!'], 400);
           }
        }else{
            return response(['error' => 'Transaction not found, please collect again'], 401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
